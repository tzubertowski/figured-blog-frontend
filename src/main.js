// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Vuex from 'vuex'

Vue.use(BootstrapVue);
Vue.use(Vuex);
Vue.config.productionTip = false;

/* eslint-disable no-new */
const store = new Vuex.Store({
    state: {
        blogApiUri: "http://api.blog.dev.com/api/posts",
        apiTokenUri: "http://auth.blog.dev.com/oauth/token",
        authClientSecret: "DJP2TGfrPkyZzDe80Ve3Sq4DZNpfVaB8lyvvn0D2",
        loggedIn: false,
        token: ""
    },
    mutations: {
        increment (state) {
            state.count++
        },
        isLoggedIn (state) {
            state.loggedIn = true
        },
        setToken (state, token) {
            state.token = token
        }
    }
})

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})